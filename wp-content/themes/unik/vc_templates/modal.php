<?php

/****************************Modal BOX****************/

vc_map( array(
   "name" => __("Unik Modal box", "unik"),
   "base" => "reveal",
   "class" => "",
   "icon" => "icon-wpb-my_reveal",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "unik"),
   "params" => array(
       array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => __("Color", "unik"),
         "param_name" => "color",
         "value" => array("Default"=>"btn-default","Primary"=>"btn-primary", "Info"=>"btn-info", "Success"=>"btn-success","Warning"=>"btn-warning", "Danger"=>"btn-danger"),
         "description" => __("Choose button color.", "unik")
      ),
      array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => __("Size", "unik"),
         "param_name" => "size",
         "value" => array("Large"=>"btn-lg", "Default"=>"", "Small"=>"btn-sm","Very Small"=>"btn-xs"),
         "description" => __("Choose button size.", "unik")
      ),
      
      array(
         "type" => "textfield",
         "holder" =>'div',
         "class" => "",
         "heading" => __("Button Text", "unik"),
         "param_name" => "button"
      ),
      array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => __("Box Title", "unik"),
         "param_name" => "revtitle"
      ),
       array(
         "type" => "textarea_html",
         
         "class" => "",
         "heading" => __("Content", "unik"),
         "param_name" => "content",
         "description" => __(" Box content.", "unik")
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "unik"),
         "param_name" => "class",
         "description" => __(' Extra class name', "unik")
      )
   )
) );

/************************************************/
?>