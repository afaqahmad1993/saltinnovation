<?php
/***************** TEAM MEMBERS *******************/
function vc_teammember($atts, $content=NULL){
    extract(shortcode_atts(array(
        'name'=>'',
		'position' => '',
        'phone'=>'',
		'email'=>'',
		'fax'=>'',
		'photo'=>'',
        'desc'=>'',
		'urlcaption' => 'Details',
        'anim'=>'',
        'class'=>'',
		'fb'=>'',
		'tw'=>'',
		'gpl'=>'',
		'drb'=>'',
    ), $atts));
	
	$image = wp_get_attachment_image_src($photo, 'full');
	
	$anim=(!empty($anim)) ? 'animation '.$anim : '';
		do_shortcode ($content);
		$out= '<div class="team-post '.$anim.' '.$class.'">
				<div class="left-part">
					<img alt="'.$name.'" src="' . $image[0] . '" class="img-responsive">
					<ul class="social-team">';
							if($gpl) $out.= '<li><a href="'.$gpl.'" title="Google+" class="google-plus"><i class="fa fa-google-plus"></i></a></li>';
							if($drb) $out.= '<li><a href="'.$drb.'" title="dribbble" class="dribbble"><i class="fa fa-dribbble"></i></a></li>';
							if($tw) $out.= '<li><a href="'.$tw.'" title="Twitter" class="twitter"><i class="fa fa-twitter"></i></a></li>';
							if($fb) $out.= '<li><a href="'.$fb.'" title="Facebook" class="facebook"><i class="fa fa-facebook "></i></a></li>';	
				$out.='</ul>
				</div>
				<div class="right-part">
					<h3>'.$name.'</h3>';
					if($position) $out.='<p class="mposition"> '.$position.'</p>';
					$out.='<ul class="contact-info">';
					if($phone) $out.='<li><i class="fa fa-phone"></i> '.$phone.'</li>';
					if($email) $out.='<li><i class="fa fa-envelope"></i> <a href="mailto:'.$email.'">'.$email.'</a></li>';
					if($fax) $out.='<li><i class="fa fa-print"></i>'.$fax.'</li>';
					$out.='</ul>
					<a href="'.$desc.'">'.$urlcaption.'</a>
				</div>
			</div>';
    return $out;
}

add_shortcode('vc_teammember', 'vc_teammember');
/*****************************************************/
vc_map( array(
   "name" => __("Unik Team Member", "unik"),
   "base" => "vc_teammember",
   "class" => "",
   "icon" => "icon-wpb-my_teammember",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "unik"),
   "params" => array(
        array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Animation", "unik"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => __(" Animation.", "unik")
      ),
      array(
         "type" => "textfield",
         "holder"=>"div",
         "class" => "",
         "heading" => __("Name", "unik"),
         "param_name" => "name"
      ),
      array(
         "type" => "textfield",
         
         "class" => "",
         "heading" => __("Position", "unik"),
         "param_name" => "position"
      ),
       array(
         "type" => "attach_image",
         
         "class" => "",
         "heading" => __("Photo ", "unik"),
         "param_name" => "photo"
      ),
	   
        array(
         "type" => "textfield",     
         "class" => "",
         "heading" => __("Link to Facebook profile(optional)", "unik"),
         "param_name" => "fb"
      ),
        array(
         "type" => "textfield",  
         "class" => "",
         "heading" => __("Link to Twitter profile(optional)", "unik"),
         "param_name" => "tw"
      ),
       
        array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("Link to Google+ profile(optional)", "unik"),
         "param_name" => "gpl"
      ),
	    array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("Link to Dribble profile(optional)", "unik"),
         "param_name" => "drb"
      ),
	    array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("Phone", "unik"),
         "param_name" => "phone"
      ),
	    array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("E-mail address", "unik"),
         "param_name" => "email"
      ),
	    array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("Fax", "unik"),
         "param_name" => "fax"
      ),
	    array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("Link to member description", "unik"),
         "param_name" => "desc"
      ),
	    array(
         "type" => "textfield",    
         "class" => "",
         "heading" => __("Link caption", "unik"),
         "param_name" => "urlcaption"
      ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "unik"),
         "param_name" => "class",
         "description" => __(' Extra class name', "unik")
      ),
   )
) );
/*********************************************/
?>