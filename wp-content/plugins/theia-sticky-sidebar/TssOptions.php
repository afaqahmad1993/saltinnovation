<?php
/*
 * Copyright 2013, Theia Sticky Sidebar, Liviu Cristian Mirea Ghiban.
 */

class TssOptions {
	public static function get($optionId) {
		$groups = array('tss_general');
		foreach ($groups as $groupId) {
			$options = get_option($groupId);
			if (array_key_exists($optionId, $options)) {
				return $options[$optionId];
			}
		}
		return null;
	}

	// Initialize options
	public static function initOptions() {
		$defaults = array(
			'tss_general' => array(
				'currentTheme' => '',
				'isTemplate' => false,
				'sidebarSelector' => '',
				'containerSelector' => '',
				'additionalMarginTop' => '',
				'additionalMarginBottom' => '',
				'minWidth' => '0',
				'updateSidebarHeight' => false,
				'disableOnHomePage' => false,
				'disableOnCategoryPages' => false,
				'disableOnPages' => false,
				'disableOnPosts' => false
			)
		);

		foreach ($defaults as $groupId => $groupValues) {
			$options = get_option($groupId);
			$changed = false;

			// Add missing options
			foreach ($groupValues as $key => $value) {
				if (isset($options[$key]) == false) {
					$changed = true;
					$options[$key] = $value;
				}
			}

			// Save options
			if ($changed) {
				update_option($groupId, $options);
			}
		}

		// Are the current settings for a different theme? If there is a template available, then use it.
		if (
			self::get('currentTheme') != wp_get_theme()->Name &&
			TssTemplates::getTemplate() !== null
		) {
			TssTemplates::useTemplate();
		}
	}
}