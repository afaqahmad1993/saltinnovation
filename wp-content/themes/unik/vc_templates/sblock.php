<?php
/************ Service BLOCK****************/

/**************************************************/
vc_map( array(
   "name" => __("Unik Service block", "unik"),
   "base" => "sblock",
   "class" => "",
   "icon" => "icon-wpb-my_promo",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "unik"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Animation", "unik"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => __(" Animation", "unik")
      ),
      array(
         "type" => "dropdown",
         
         "class" => "",
         "heading" => __("Type", "unik"),
         "param_name" => "type",
         "value"=>array("Type 1"=>"1", "Type 2"=>"2", "Statistic"=>"stat")
      ),
	   array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Custom color", "unik"),
            "param_name" => "color",
            "value" => '',
			"description" => __('Only for Type 1', "unik")
         ),
      array(
         "type" => "textfield",
		  "holder"=>"div",
         "class" => "",
         "heading" => __("Block title", "unik"),
         "param_name" => "title"
      ),
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Block icon", "unik"),
         "param_name" => "icon"
	 ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Statistic count", "unik"),
         "param_name" => "count",
         "description" => __('Only for Statistic Type', "unik")
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Block Link", "unik"),
         "param_name" => "link"
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Block link Caption", "unik"),
         "param_name" => "linkcaption"
      ),
       array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "unik"),
         "param_name" => "class",
         "description" => __(' Extra class name', "unik")
      )
   )
) );
/*********************************************/
?>