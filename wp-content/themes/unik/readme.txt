Refer to documentation folder on download package for theme documentation.

Version 1.1 List of changes:

Added Full width layout option
Added RTL support
Added WooCommerce compatibility
Added WPML compatibility
Added 4 more social services in Social widget
Added option to carousel shortcodes to hide arrows
Fixed blog shortcode not synchronizing with parameters set via theme admin panel bug
Fixed dark version not taking some parameters set by live customizer bug
Fixed some live customizer issues
Fixed single posts page showing featured image in title header bug
Fixed bug with menu shadow being assigned white color by default
Fixed carousel script issue
Fixed portfolio shortcode magnify issue
Fixed some missing strings from translation


Version 1.2 List of changes:

Added full support for post formats
Added Visual composer plugin to the package, full integration coming soon
Added option to show logo on under construction page
Added speed parameter option for the slider
Added background color choice option to full width block
Added custom bg/color to button and set target _self as default
Added option to set page heading image for blog archives
Changed Blog layout 2 to support isotope
Fixed social icons widget target window issue
Fixed contact page content not showing bug
Fixed issue with header box appearing if title/breadcrumb is set to hidden and there is no featured image
Fixed main sidebar widgets not showing on mobile resolutions bug
Fixed some styles not being applied when set via customizer
Fixed featured image not showing when set from post head options bug
Fixed sticky menu on mobile views issue (now it's sticky on desktop resolutions only)
Fixed contact widget sending issues by switching to wp_mail() function
Fixed Woocommerce page sidebar not appearing when set bug
Fixed youtube and vimeo social buttons issues


Version 1.3 List of changes:

Fixed theme admin panel incompatibility issue with Wordpress 4.0
Fixed dropdown menu on mobile not showing correctly bug
Fixed page with custom featured image in header not showing along with title/breadcrumb bug.
Minor CSS fixes



Version 1.4 List of changes:

Added full compatibility with Visual composer
Fixed pagination issue with blog set as main page
Fixed some issues with shortcodes
Added Contact Form 7 styles and sample markup
Updated Font awesome to version 4.3
Updated Visual composer plugin to the latest version
Fixed outdated bundled files issue with Woocommerce 2.2+
Fixed some layout issues with Fluid version of the theme
Minor fixes and improvements



Version 1.5 List of changes:

Added a 1-click installer for blazing-fast setup
Updated Visual composer plugin to the latest version
Added a sample content file for Visual composer variation
Updated social links styles to use corresponding brands colors
Fixed portfolio items zoom not working issue
Fixed portfolio featured items shortcode parameter not working with latest version of WP issue.
Fixed incompatibility issues with latest version of Woocommerce



Version 1.6 List of changes:
----------------------------
Full comatibility with WP 4.3+
Fixed issues with visual composer
Fixed outdated woocommerce files error
Added automatic Plugin activation option for easier setup
Updated Font Awesome to version 4.4.0
Updated all the plugins to the latest versions
Updated the sample contents file for Visual composer to reflect the latest changes
Minor fixes and enhancements