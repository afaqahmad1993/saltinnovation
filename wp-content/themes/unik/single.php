<?php /** The Template for displaying all single posts. **/

get_header();
$get_meta = get_post_custom($post->ID);

$overridepos = isset($get_meta['_weblusive_sidebar_pos']) ? $get_meta['_weblusive_sidebar_pos'] : '';
$overridepos = (empty($overridepos)) ? 'default' : $overridepos[0];
$overridesidebar =  isset($get_meta['_weblusive_sidebar_post']) ? $get_meta['_weblusive_sidebar_post'] : '';
$overridesidebar = (empty($overridesidebar)) ? '' : $overridesidebar[0];

$sidebarPos = ($overridepos == 'default') ?  weblusive_get_option('sidebar_pos') : $overridepos;
$sidebar = ($overridesidebar == '') ?  weblusive_get_option('sidebar_post') : $overridesidebar;

$showdate = weblusive_get_option('blog_show_date');
$showcomments = weblusive_get_option('blog_show_comments');
$showauthor = weblusive_get_option('blog_show_author');
$format =  get_post_format( $post->ID );
if (empty($format)) $format = 'standard';
get_template_part( 'library/includes/page-head' );
get_template_part( 'inner-header', 'content');
?>

<div class="page-section row">
	<?php if (!empty($sidebar) && $sidebarPos=='left') :?><div class="col-md-4 side-div" ><?php get_sidebar($sidebar)?></div><?php endif; ?>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="<?php if (empty($sidebar) || $sidebarPos == 'full'):?>col-md-12<?php else:?>col-md-8<?php endif?>">

		<div class="single-post-container post-format-<?php echo $format?>">
			<?php
			$get_meta = get_post_custom($post->ID);
			$mediatype = isset($get_meta["_blog_mediatype"]) ? $get_meta["_blog_mediatype"][0] : 'image';
			$videoId = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : '';
			$autoplay =  isset($get_meta["_blog_videoap"]) ? $get_meta["_blog_videoap"][0] : '0';
			$thumbnail = get_the_post_thumbnail($post->ID, 'blog-single');
			?>
			<?php if ($format == 'standard' || $format == 'video' || $format == 'image' || $format == 'gallery'):?>
				<div class="post-media">
					<?php if($format == 'standard' || $format == 'image'):
						$noimage = (isset($get_meta['_weblusive_post_head'][0]) && $get_meta['_weblusive_post_head'][0] == 'none') ? true : false;
						?>
						<?php $thumbnail = get_the_post_thumbnail(); if(!empty($thumbnail)) : ?>
							<?php the_post_thumbnail('full'); ?>
						<?php endif?>
					<?php elseif ($format == 'video'):
						global $wp_embed;
						$post_embed = '';
						$video = isset($get_meta["_blog_video"]) ? $get_meta["_blog_video"][0] : '';
						$videoself = isset($get_meta["_blog_video_selfhosted"]) ? $get_meta["_blog_video_selfhosted"][0] : '';
						if ($video || $videoself): ?>
							<div class="flex-video">
								<?php
									if ($video):
										$post_embed = $wp_embed->run_shortcode('[embed width="850" height="500"]'.$video.'[/embed]');
									else:
										$post_embed = $wp_embed->run_shortcode($videoself);
									endif;
									echo $post_embed;
								?>
							</div>
						<?php else:?>
							<div class="alert alert-danger fade in">
								<?php _e('Video post format was chosen but no url or embed code provided. Please fix this by providing it.', 'unik') ?>
							</div>
						<?php endif?>
					<?php elseif ($format == 'gallery'):
						$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full', false);
						$argsThumb = array(
							'order'          => 'ASC',
							'posts_per_page'  => 99,
							'post_type'      => 'attachment',
							'post_parent'    => $post->ID,
							'post_mime_type' => 'image',
							'post_status'    => null,
							//'exclude' => get_post_thumbnail_id()
						);
						?>
						<div class="simple-slider">
							<ul class="slides">
								<?php
								$attachments = get_posts($argsThumb);
								if ($attachments) {
									foreach ($attachments as $attachment) {
										$image = wp_get_attachment_url($attachment->ID, 'full', false, false);
										$alt = $attachment->post_excerpt;
										echo '<li><img src="'.$image.'" alt="'.$alt.'" class="img-responsive"></li>';
									}
								}
								?>
							</ul>
						</div>
					<?php endif?>
				</div>
			<?php endif?>
			<div class="single-post-content">
				<h1><?php the_title()?></h1>
				<ul class="post-tags">
					<?php if(!$showdate): ?>
						<li>
							<i class="fa fa-calendar-o"></i>
							<span><?php echo get_the_time('M d, Y'); ?></span>
						</li>
					<?php endif; ?>
					<?php if(!$showauthor): ?>
						<li>
							<i class="fa fa-user"></i>
							<span><?php the_author() ?></span>
						</li>
					<?php endif; ?>
					<?php if( 'open' == $post->comment_status && !$showcomments) : ?>
						 <li>
							 <i class="fa fa-comments"></i>
							 <?php comments_popup_link( __( '0 comment', 'unik' ), __( '1 comment', 'unik' ), __( '% comments', 'unik' )); ?>
						 </li>
					<?php endif?>
				</ul>
				<?php the_content()?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'unik' ), 'after' => '</div>' ) ); ?>


			</div>
		</div>
		<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>		
		
	</div>
    <?php endwhile; ?>
	<?php if (!empty($sidebar) && $sidebarPos=='right') :?><div class="col-md-4 side-div" ><?php get_sidebar($sidebar)?></div><?php endif; ?>
</div>
<?php get_footer(); ?>