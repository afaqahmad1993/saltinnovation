
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_es_deliverreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_es_deliverreport` (
  `es_deliver_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `es_deliver_sentguid` varchar(255) NOT NULL,
  `es_deliver_emailid` int(10) unsigned NOT NULL,
  `es_deliver_emailmail` varchar(255) NOT NULL,
  `es_deliver_sentdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_deliver_status` varchar(25) NOT NULL,
  `es_deliver_viewdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `es_deliver_sentstatus` varchar(25) NOT NULL DEFAULT 'Sent',
  `es_deliver_senttype` varchar(25) NOT NULL DEFAULT 'Immediately',
  PRIMARY KEY (`es_deliver_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_es_deliverreport` WRITE;
/*!40000 ALTER TABLE `wp_es_deliverreport` DISABLE KEYS */;
INSERT INTO `wp_es_deliverreport` VALUES (1,'nwgphj-nhxsdi-gjbace-stmnux-wqngmk',1,'afaq.dev@iafrica.tv','2017-08-17 12:42:57','Viewed','2017-08-17 12:56:03','Sent','Immediately'),(2,'kvlntj-owhmki-qzxnca-hyerpl-yepuob',1,'afaq.dev@iafrica.tv','2017-08-23 11:05:54','Nodata','0000-00-00 00:00:00','Sent','Immediately'),(3,'lszoux-jvxgyr-hdtmbq-nxzfgp-oirdxc',1,'afaq.dev@iafrica.tv','2017-08-23 11:12:58','Nodata','0000-00-00 00:00:00','Sent','Immediately'),(4,'cnkstw-lcghrs-abyoqs-mcsakn-etywzk',1,'afaq.dev@iafrica.tv','2017-08-23 11:20:31','Nodata','0000-00-00 00:00:00','Sent','Immediately'),(5,'egwpif-skitqc-yahruf-qefkhu-fpuaks',1,'afaq.dev@iafrica.tv','2017-08-23 11:23:12','Nodata','0000-00-00 00:00:00','Sent','Immediately'),(6,'olxthn-zuevkm-ytkejz-twovkm-rvdelz',1,'afaq.dev@iafrica.tv','2017-08-23 11:25:08','Nodata','0000-00-00 00:00:00','Sent','Immediately'),(7,'dvignh-vloiar-mxupyw-fmuoqe-ynvbaq',1,'afaq.dev@iafrica.tv','2017-08-23 15:30:39','Nodata','0000-00-00 00:00:00','Sent','Immediately');
/*!40000 ALTER TABLE `wp_es_deliverreport` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

