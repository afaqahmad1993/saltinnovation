<?php
/*
 * Copyright 2013, Theia Sticky Sidebar, Liviu Cristian Mirea Ghiban.
 */

class TssTemplates {
	public static function getTemplate() {
		$defaults = array(
			'sidebarSelector' => '',
			'containerSelector' => '',
			'additionalMarginTop' => 0,
			'additionalMarginBottom' => 0,
			'minWidth' => 0,
			'updateSidebarHeight' => false
		);

		$templates = array(
			'Twenty Eleven' => array(
				'sidebarSelector' => '#secondary'
			),
			'Twenty Twelve' => array(
				'sidebarSelector' => '#secondary'
			),
			'Twenty Thirteen' => array(
				'sidebarSelector' => '.sidebar-inner > .widget-area',
				'containerSelector' => '#main',
				'additionalMarginTop' => 24
			),
			'Blue Diamond' => array(
				'sidebarSelector' => '.sidebar-wrapper'
			),
			'Bolid Theme' => array(
				'sidebarSelector' => '#secondary'
			),
			'Boutique Shop' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 30
			),
			'Churchope' => array(
				'sidebarSelector' => '.left-sidebar, .right-sidebar'
			),
			'Complexity' => array(
				'sidebarSelector' => '#secondary'
			),
			'deTube' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 10
			),
			'Directory' => array(
				'sidebarSelector' => '#secondary'
			),
			'Twenty Ten' => array(
				'sidebarSelector' => '#primary',
				'additionalMarginTop' => 20
			),
			'Jarida' => array(
				'sidebarSelector' => 'aside.sidebar, aside.sidebar-narrow',
				'additionalMarginTop' => 20
			),
			'Attitude' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 20
			),
			'BoldR Lite' => array(
				'sidebarSelector' => '#sidebar-container',
				'additionalMarginTop' => 20
			),
			'Catch Box' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 20
			),
			'Clean Retina' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 20
			),
			'Coraline' => array(
				'sidebarSelector' => '#primary',
				'additionalMarginTop' => 18
			),
			'Custom Community' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Customizr' => array(
				'sidebarSelector' => '.tc-sidebar',
				'additionalMarginTop' => 20
			),
			'Destro' => array(
				'sidebarSelector' => '#sidebar_section'
			),
			'discover' => array(
				'sidebarSelector' => '#sidebar',
				'containerSelector' => '#content_container',
				'additionalMarginTop' => 20
			),
			'Eclipse' => array(
				'sidebarSelector' => '#secondary'
			),
			'EvoLve' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 20
			),
			'Expound' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Graphene' => array(
				'sidebarSelector' => '.sidebar',
				'additionalMarginTop' => 10
			),
			'iFeature' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 30
			),
			'Leaf' => array(
				'sidebarSelector' => '#secondary'
			),
			'Mantra' => array(
				'sidebarSelector' => '#primary'
			),
			'PageLines' => array(
				'sidebarSelector' => '#sidebar-wrap'
			),
			'Parament' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Pinboard' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Responsive' => array(
				'sidebarSelector' => '#widgets',
				'disableOnCss' => array(
					'float' => 'none'
				)
			),
			'Sunny Blue Sky' => array(
				'sidebarSelector' => '.sidebar',
				'additionalMarginTop' => 10
			),
			'Weaver II' => array(
				'sidebarSelector' => '#sidebar_wrap_left, #sidebar_wrap_right'
			),
			'WP Opulus' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Ego' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 40,
				'additionalMarginBottom' => 40
			),
			'Evermore' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 30,
			),
			'Flexform' => array(
				'sidebarSelector' => 'aside.sidebar',
				'additionalMarginTop' => 20
			),
			'Food Recipes' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Fundify' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 80
			),
			'FUZZY' => array(
				'sidebarSelector' => '.post-single-sidebar',
				'containerSelector' => '.arround',
				'additionalMarginTop' => 130
			),
			'Hitched' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 20
			),
			'I Love It!' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 20
			),
			'Impression' => array(
				'sidebarSelector' => '.sidebar'
			),
			'INOVADO' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 30
			),
			'JPhotolio' => array(
				'sidebarSelector' => '.sidebar',
				'additionalMarginTop' => 10,
				'additionalMarginBottom' => 40
			),
			'Kaleido' => array(
				'sidebarSelector' => '.sidebar-wrap'
			),
			'Kallyas' => array(
				'sidebarSelector' => '#mainbody > .row > .span3',
				'containerSelector' => '#mainbody'
			),
			'London Creative +' => array(
				'sidebarSelector' => '#sidebar_home',
				'additionalMarginTop' => 10
			),
			'Maya Shop' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Mural' => array(
				'sidebarSelector' => '#secondary',
				'additionalMarginTop' => 30
			),
			'Nimble' => array(
				'sidebarSelector' => '#aside'
			),
			'OneTouch' => array(
				'sidebarSelector' => '#left-sidebar, #right-sidebar',
				'additionalMarginTop' => 20
			),
			'Radial Premium Theme' => array(
				'sidebarSelector' => '.sidebar',
				'additionalMarginTop' => 10
			),
			'Responsive Fullscreen Studio for WordPress' => array(
				'sidebarSelector' => '.sidebar-wrap-single',
				'updateSidebarHeight' => true
			),
			'RT-Theme Seventeen' => array(
				'sidebarSelector' => '.sidebar',
				'additionalMarginTop' => 15
			),
			'Skinizer' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 25
			),
			'Smart IT' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 25
			),
			'Sommerce' => array(
				'sidebarSelector' => '#sidebar'
			),
			'SuperMassive' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 10
			),
			'Symplex' => array(
				'sidebarSelector' => '#sidebar',
				'updateSidebarHeight' => true
			),
			'Terra' => array(
				'sidebarSelector' => '#sidebar'
			),
			'The Agency' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Typegrid' => array(
				'sidebarSelector' => '.sidebar'
			),
			'U-Design' => array(
				'sidebarSelector' => '#sidebar',
				'additionalMarginTop' => 10
			),
			'Victoria' => array(
				'sidebarSelector' => '#sidebar'
			),
			'TrulyMinimal' => array(
				'sidebarSelector' => '#sidebar'
			),
			'Sterling' => array(
				'sidebarSelector' => 'aside.sidebar'
			),
			'Enfold' => array(
				'updateSidebarHeight' => true
			),
			'SimpleMag' => array(
				'sidebarSelector' => 'jQuery(".sidebar").parent()',
				'containerSelector' => '#content > article > .wrapper > .grids'
			),
			'POST' => array(
				'sidebarSelector' => '.sidebar-main',
				'containerSelector' => '#main'
			)
		);

		$themeName = wp_get_theme()->Name;
		$foundId = null;

		// Search for an exact match.
		if (array_key_exists($themeName, $templates)) {
			$foundId = $themeName;
		}
		else {
			// Search for prefixes.
			foreach ($templates as $templateName => $templateData) {
				if (strtolower(substr($themeName, 0, strlen($templateName))) == strtolower($templateName)) {
					$foundId = $templateName;
					break;
				}
			}
		}

		// Template found.
		if ($foundId !== null) {
			return array_merge($defaults, $templates[$foundId]);
		}

		// No match.
		return null;
	}

	public static function useTemplate() {
		$defaults = array(
			'sidebarSelector' => '',
			'currentTheme' => wp_get_theme()->Name,
			'isTemplate' => true
		);

		$template = self::getTemplate();
		if ($template !== null) {
			$template = array_merge(array_merge(get_option('tss_general'), $defaults), $template);

			update_option('tss_general', $template);
			TssOptions::initOptions();
		}
	}

	// Are the current settings different from the theme's template?
	public static function areSettingsDifferentFromTemplate() {
		$template = self::getTemplate();

		if ($template === null) {
			return false;
		}

		foreach ($template as $key => $value) {
			if ($value != TssOptions::get($key)) {
				return true;
			}
		}

		return false;
	}
}
