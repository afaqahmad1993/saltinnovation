<?php /* Template Name: Contact Form w/CF7 plugin */ 

get_header();

$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
get_template_part( 'library/includes/page-head' ); 
get_template_part( 'inner-header', 'content'); 
$options = array(
		weblusive_get_option('contact_error'), 
		weblusive_get_option('contact_success'),
		weblusive_get_option('contact_subject'), 
		weblusive_get_option('contact_email'), 		
	);
$address = weblusive_get_option('contact_address'); 
if (!empty($address)):?>
<script type="text/javascript">   
jQuery(function(){
	jQuery('#map').gmap3({
		action: 'addMarker',
		address: "<?php echo htmlspecialchars($address)?>",
		infowindow:{
		options:{
			content: "<?php echo $address?>"
			},
			},
		map:{
			center: true,
			zoom: 14,
			},
		
		},
		{action: 'setOptions', args:[{scrollwheel:false}]}		
		);	  
	});
</script> 
<?php endif?> 
<?php if (!empty($address)):?>
	<div class="box-section map-section">
		<h2><?php _e('OUR LOCATION', 'unik')?></h2>
		<div id="map" class="gmap3 map_location map"></div>
	</div>
<?php endif?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="box-section contact-section">
	<?php the_content() ?>
	<?php echo do_shortcode('[contact-form-7 id="1831" title="Contact form 1"]'); ?>
</div>
<?php endwhile; ?>	

<?php get_footer(); ?>