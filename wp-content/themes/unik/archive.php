<?php get_header();

$sidebar = weblusive_get_option('sidebar_archive');
$sidebarPos =  weblusive_get_option('sidebar_pos');
if (empty ($sidebar)) $sidebarPos = 'full';
$blogLayout =  weblusive_get_option('blog_layout');
isset ($blogLayout) ? $blogLayout : $blogLayout==1;
?>
<div class="row">
	<div class="col-md-12">
		<?php $thumbnail = weblusive_get_option('archives_img'); if(!(weblusive_get_option('hide_titles') && weblusive_get_option('hide_breadcrumbs'))):?>
			<div class="box-section banner-section">
				<div class="banner">
					<?php 
						if(!empty($thumbnail) && !is_single()) {
							echo '<img src="'.$thumbnail.'" alt="banner" />';
						} 
					?>
					<?php if (!weblusive_get_option('hide_titles')):?>
						<h1 class="page-title">
							<span>
								<?php if ( is_day() ) : ?>
								<?php printf( __( 'Daily Archives: %s', 'unik' ), get_the_date() ); ?>
								<?php elseif ( is_month() ) : ?>
								<?php printf( __( 'Monthly Archives: %s', 'unik' ), get_the_date('F Y') ); ?>
								<?php elseif ( is_year() ) : ?>
								<?php printf( __( 'Yearly Archives: %s', 'unik' ), get_the_date('Y') ); ?>
								<?php elseif ( is_category() ) : ?>
								<?php single_cat_title();?>
								<?php else : ?>
								<?php _e( 'Blog Archives', 'unik' ); ?>
								<?php endif; ?>
							</span>
						</h1>
					<?php endif; ?>
				</div>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<div class="pager-line">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif?>
	</div>
</div>
<div class="blog-section <?php echo ($blogLayout==1) ? 'col1' : 'col2 blog-isotope'; ?> page-section row">
	<?php if ($sidebarPos == 'left'):?><div class="col-md-4 side-div" ><?php get_sidebar($sidebar); ?></div><?php endif?>
    <div class="<?php if ($sidebarPos == 'full' || empty($sidebar)):?>col-md-12<?php else:?>col-md-8<?php endif?>">
		<?php
			if ( have_posts() ) the_post();
			rewind_posts();       
			get_template_part( 'loop', 'archive' );
		?>
		
	</div>
	
	<?php if ($sidebarPos == 'right'):?><div class="col-md-4 side-div" ><?php get_sidebar($sidebar); ?></div><?php endif?>
</div>
<?php if ( $wp_query->max_num_pages > 1 ): ?>
	<div class="pagination-list">
		<?php include(unik_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
	</div>
<?php endif?>
<?php get_footer(); ?>