<?php
/****************** content carousel ********************/
class WPBakeryShortCode_Carousel extends WPBakeryShortCodesContainer{

}

/***/
class WPBakeryShortCode_Caritem extends WPBakeryShortCode {
    
}


/****/
vc_map( array(
   "name" => __("Unik Content Carousel", "unik"),
	"show_settings_on_create" => true,
	'is_container' => true,
	"content_element" => true,
	"as_parent" => array('only' => 'caritem'),
   "base" => "carousel",
   "class" => "",
   "icon" => "icon-wpb-my_av_carousel",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "unik"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Animation", "unik"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => __(" Animation.", "unik")
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Type", "unik"),
         "param_name" => "type",
         "value" => array("Default"=>"", "Brand slider"=>"brands")
      ), 
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Autoplay", "unik"),
         "param_name" => "automatic",
         "value" => array("No"=>"false", "Yes"=>"true")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Autoplay speed(milliseconds)", "unik"),
         "param_name" => "interval",
         "value" => '4000'
      ),
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Show Navigation Arrows", "unik"),
         "param_name" => "showarrows",
         "value" => array("Yes"=>"true", "No"=>"false")
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Min. visible Items", "unik"),
         "param_name" => "min",
         "value" => '1'
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Max. visible Items", "unik"),
         "param_name" => "max",
         "value" => '6'
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Slide width", "unik"),
         "param_name" => "slwidth",
         "value" => '257'
      ),
	  
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Slide margin", "unik"),
         "param_name" => "slmargin",
         "value" => ''
      ),
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "unik"),
         "param_name" => "class",
         "description" => __(' Extra class name', "unik")
      ),
   ),
	"js_view" => 'VcColumnView'
) );
/**********************************************************/
vc_map( array(
	'name' => __( 'Carousel Item', 'unik' ),
	'base' => 'caritem',
	"icon" => "icon-wpb-ui-accordion",
	"as_child" => array('only' => 'carousel'),
	'content_element' => true,
	'params' => array(
		array(
		 "type" => "textarea_html",
		  "holder"=>"div",	
		  "class" => "",
		  "heading" => __("Content", "unik"),
		  "param_name" => "content"
		 ),		
		)
	))
?>
