<?php
/****************** SLIDER ********************/
class WPBakeryShortCode_Slider extends WPBakeryShortCodesContainer {
	
}


/*******************************/
class WPBakeryShortCode_Slideritem extends WPBakeryShortCode {
	
}

/****/
vc_map( array(
   "name" => __("Unik Image Slider", "unik"),
   "base" => "slider",
	"as_parent" => array('only' => 'slideritem'),
   "show_settings_on_create" 	=> true,
   "class" => "",
   "icon" => "icon-wpb-my_slider",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "unik"),
	"content_element" => true,
	"is_container"=> true,
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Animation", "unik"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => __(" Animation.", "unik")
      ),
	   
	  
	   array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Autoplay", "unik"),
         "param_name" => "automatic",
         "value" => array( "No"=>"false", "Yes"=>"true")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Autoplay speed(milliseconds)", "unik"),
         "param_name" => "interval",
         "value" => '7000'
      ),
	    
      array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "unik"),
         "param_name" => "class",
         "description" => __(' Extra class name', "unik")
      ),
   ),
"js_view" => 'VcColumnView'
) );
/************************************************/
vc_map( array(
	"name" => __("Slider Item", "unik"),
	"base" => "slideritem",
	"class" => "",
	"content_element" => true,
	"icon" => "icon-wpb-ui-accordion",
	"category" => __('Content', "unik"),
	"as_child" => array('only' => 'slider'),
	"params" => array(
      array(
         "type" => "textfield",
		 'holder'=>'div',
         "class" => "",
         "heading" => __("Name", "unik"),
         "param_name" => "title"
      ),
       
		
       array(
         "type" => "attach_image",
         "class" => "",
         "heading" => __("Image ", "unik"),
         "param_name" => "image"
      ),
   )
) );
?>