<div class="row">
	<div class="col-md-12">
		<?php $thumbnail = get_the_post_thumbnail(); 
			$globalbg =  get_theme_mod('title_content_bg_image');
			
			if(!((weblusive_get_option('hide_titles') && weblusive_get_option('hide_breadcrumbs')))):?>
			<div class="box-section banner-section">
				<div class="banner">
					<?php 
						if(!empty($thumbnail) && !is_single()) {
							the_post_thumbnail();
						} 
					?>
					<?php if (!weblusive_get_option('hide_titles')):?>
						<h1 class="page-title"><span><?php the_title(); ?></span></h1>
					<?php endif; ?>
					
				</div>
				<?php if (!weblusive_get_option('hide_breadcrumbs')):?>
					<div class="pager-line">
						<?php if(class_exists('the_breadcrumb')){ $albc = new the_breadcrumb; } ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>