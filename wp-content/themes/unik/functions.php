<?php
/******************* DEFINE NAME/VERSION ********************/

$themename = "Unik";
$themefolder = "unik";

define ('theme_name', $themename );
define ('theme_ver' , 1 );

/************************************************************/

/********************* DEFINE MAIN PATHS ********************/
define('unik_SHORTCODES',  get_template_directory() . '/library/functions/shortcodes/ui.php' ); // Shortcut to the /addons/ directory
define('unik_PLUGINS',  get_template_directory() . '/addons' ); // Shortcut to the /addons/ directory


/*-----------------------------------------------------------------------------------*/
$adminPath 	=  get_template_directory() . '/library/admin-panel/';
$funcPath 	=  get_template_directory() . '/library/functions/';
$incPath 	=  get_template_directory() . '/library/includes/';
$tgmPath 	=  get_template_directory() . '/installer/plugin-activation/';

require_once ($funcPath . 'helper-functions.php');
require_once ($incPath . 'the_breadcrumb.php');
require_once ($incPath . 'OAuth.php');
require_once ($incPath . 'twitteroauth.php');
require_once ($incPath . 'portfolio_walker.php');
require_once ($funcPath . 'post-types.php');
require_once ($tgmPath . 'class-tgm-plugin-activation.php');
require_once ($funcPath . 'widgets.php');

include (get_template_directory() . '/library/admin-panel/admin-ui.php');
include (get_template_directory() . '/library/admin-panel/admin-functions.php');
include (get_template_directory() . '/library/admin-panel/post-options.php');
include (get_template_directory() . '/library/admin-panel/custom-slider.php');
include (get_template_directory() . '/library/customizer/customizer.php');

require get_template_directory() .'/installer/init.php';

/************************************************************/


/*********** LOAD ALL REQUIRED SCRIPTS AND STYLES ***********/
function unik_load_scripts()
{
	// Register or enqueue scripts
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap',  get_template_directory_uri(). '/js/bootstrap.js', array('jquery'), '3.0.1' );
	wp_enqueue_script('plugins', get_template_directory_uri() .'/js/plugins.js', array('jquery'), '3.2', true);
	wp_enqueue_script('scripts', get_template_directory_uri() .'/js/script.js', array('jquery'), '3.2', true);
	
	if (is_page_template('contact-template.php') || is_page_template('contact-template2.php')){
		
		$address = "";
		$address = weblusive_get_option('contact_address');
		
		$map_api = "";
		$map_api = weblusive_get_option('google_api');

		if( $address != "" && $map_api != "" ) {
			wp_enqueue_script('google-map-api',  'http://maps.google.com/maps/api/js?key='. $map_api );
			wp_enqueue_script('google-map',  get_template_directory_uri().'/js/gmap3.min.js' );
		}
		else {
			wp_enqueue_script('google-map-api',  'https://maps.googleapis.com/maps/api/js?v=3.exp' );
			wp_enqueue_script('google-map',  get_template_directory_uri().'/js/gmap3.min.js');			
		}		
		wp_enqueue_script('Validate',  get_template_directory_uri().'/js/jquery.validate.min.js', array('jquery'), '3.2', true);
	}	
	
	if (is_page_template('under-construction.php'))
	{
		wp_enqueue_script('Under-construction',  get_template_directory_uri().'/js/jquery.countdown.js', array('jquery'), '3.2', true);
	}
}

function unik_load_styles(){
	$rtl =  weblusive_get_option('rtl_mode');
	$layout =  weblusive_get_option('theme_layout');
	$mainStyle=weblusive_get_option('theme_skin');
	
	wp_enqueue_style('plugin-styles',  get_template_directory_uri().'/css/plugins.css');
	//(isset($_GET['skin']))? $mainStyle=$_GET['skin'] : $mainStyle='';
	wp_enqueue_style('main-styles', get_template_directory_uri().'/style.css');
	if(isset($mainStyle) && $mainStyle!=''){
		wp_enqueue_style('skin-styles', get_template_directory_uri().'/css/skins/'.$mainStyle.'/style.css');
	}
	//wp_enqueue_style('shotcodes_styles',  get_template_directory_uri().'/css/shotcodes_styles.css');
	wp_enqueue_style('dynamic-styles',  get_template_directory_uri().'/css/dynamic-styles.php');
	if ($rtl){
		wp_enqueue_style('bootstrap-rtl',  get_template_directory_uri().'/css/bootstrap-rtl.css');
	}
	if ($layout == 'fluid'){
		wp_enqueue_style('fullwidth-style',  get_template_directory_uri().'/style-full-width.css');
	}
	
}
add_action( 'wp_enqueue_scripts', 'unik_load_styles' );
add_action( 'wp_enqueue_scripts', 'unik_load_scripts' );


// Load Google Fonts
function unik_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'unik-roboto', "$protocol://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic" );
    wp_enqueue_style( 'unik-opensans', "$protocol://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300" );
}
add_action( 'wp_enqueue_scripts', 'unik_fonts' );

/************************************************************/


/************** THEME GENERAL SETUP FUNCTION ****************/
add_action( 'after_setup_theme', 'unik_setup' );
function unik_setup() {
	
	/**** LANGUAGE SETUP ****/
	load_theme_textdomain( 'unik',  get_template_directory() . '/languages' );
	$locale = get_locale();
	$locale_file =  get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		
	require_once( $locale_file );
	
	/**** ADD SUPPORT FOR MENUS ****/
	add_theme_support( 'menus' );
	//Register Navigations
	add_action( 'init', 'my_custom_menus' );
	function my_custom_menus() {
		register_nav_menus(
			array(
				'primary_nav' => __( 'Primary Navigation', 'unik'),
		)
		);
	}
	/*************** ADD SUPPORT FOR POST FORMATS ***************/
	
	add_theme_support('post-formats', array('image', 'gallery', 'video', 'audio', 'link', 'quote', 'status') );

	/**** ADD SUPPORT FOR POST THUMBS ****/
	add_theme_support( "title-tag" );
	add_theme_support( 'post-thumbnails');
	// Define various thumbnail sizes
	add_image_size('portfolio-3-col', 256, 163, true); 
	add_image_size('portfolio-2-col', 395, 251, true);
	add_image_size('portfolio-alt', 317, 243, true); 
	add_image_size('blog-single', 850, 424, true);
	add_image_size('blog-list', 310, 223, true);
	add_image_size('blog-thumb', 50, 50, true);
	add_image_size('blog-thumb-3', 360, 276, true);
	
	/* WooCommerce Theme Support */
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

/******* FIX THE PORTFOLIO CATEGORY PAGINATION ISSUE ********/

$option_posts_per_page = get_option( 'posts_per_page' );
add_action( 'init', 'my_modify_posts_per_page', 0);
function my_modify_posts_per_page() {
    add_filter( 'option_posts_per_page', 'my_option_posts_per_page' );
}
function my_option_posts_per_page( $value ) {
    global $option_posts_per_page;
    if ( is_tax( 'portfolio_category') ) {
		$pageId = get_page_ID_by_page_template('portfolio-template-3columns.php');
		if ($pageId)
		{
			$custom =  get_post_custom($pageId);
			$items_per_page = isset ($custom['_page_portfolio_num_items_page']) ? $custom['_page_portfolio_num_items_page'][0] : '777';
			return $items_per_page;
		}
		else
		{
			return 4;
		}
    } else {
        return $option_posts_per_page;
    }
}

/************************************************************/

function weblusive_get_option( $name ) {
	$get_options = get_option( 'weblusive_options' );
	
	if( !empty( $get_options[$name] ))
		return $get_options[$name];
		
	return false ;
}

//Docs Url
$docs_url = "http://".$themefolder.".weblusive-themes.com/documentation/";

// Redirect To Theme Options Page on Activation
if (is_admin() && isset($_GET['activated'])){
	wp_redirect(admin_url('admin.php?page=panel'));
}

/************************************************************/


/****************** REGISTER SIDEBARS ***********************/

add_filter('widget_text', 'do_shortcode');
add_filter('the_excerpt', 'do_shortcode');

add_action( 'widgets_init', 'unik_widgets_init' );
function unik_widgets_init() {
	$before_widget =  '<div id="%1$s" class="sidebar-section white-box %2$s">';
	$after_widget  =  '</div>';
	$before_title  =  '<h2>';
	$after_title   =  '</h2>';
					
	register_sidebar( array(
		'name' =>  __( 'Primary Widget Area', 'unik' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The Primary widget area', 'unik' ),
		'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
	) );
	
	
	$footer_widget_count = weblusive_get_option('footer_widgets');
	if($footer_widget_count !== 'none')
	{
		$columns = 'column3';
		switch($footer_widget_count)
		{
			case '4':
			$columns = 'col-md-3';
			break;
			case '3':
			$columns = 'col-md-4';
			break;
			case '2':
			$columns = 'col-md-6';
			break;
		}
		for($i = 1; $i<= $footer_widget_count; $i++)
		{
			unregister_sidebar('Footer Widget '.$i);
			if ( function_exists('register_sidebar') )
			register_sidebar(array(
				'name' => 'Footer Widget '.$i,
				'id'	=> 'footer-sidebar-'.$i,
				'before_widget' => '<div class="'.$columns.'"><div class=" widget footer-widgets">',
				'after_widget' => '</div></div>',
				'before_title' => '<h2>',
				'after_title' => '</h2>',
			));
		}
	}

	//Custom Sidebars
	$sidebars = weblusive_get_option( 'sidebars' ) ;
	if($sidebars){
		foreach ($sidebars as $sidebar) {
			register_sidebar( array(
				'name' => $sidebar,
				'id' => sanitize_title($sidebar),
				'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
			) );
		}
	}
}
/************************************************************/


/****************** CUSTOM LOGIN LOGO ***********************/

function unik_login_logo(){
	if( weblusive_get_option('dashboard_logo') )
    echo '<style  type="text/css"> h1 a {  background-image:url('.weblusive_get_option('dashboard_logo').')  !important; } </style>';  
}  
add_action('login_head',  'unik_login_logo'); 

/************************************************************/


/******************** CUSTOM GRAVATAR ***********************/

function unik_custom_gravatar ($avatar) {
	$weblusive_gravatar = weblusive_get_option( 'gravatar' );
	if($weblusive_gravatar){
		$custom_avatar = weblusive_get_option( 'gravatar' );
		$avatar[$custom_avatar] = "Custom Gravatar";
	}
	return $avatar;
}
add_filter( 'avatar_defaults', 'unik_custom_gravatar' ); 

/************************************************************/


/********************* CUSTOM TAG CLOUDS ********************/

function unik_custom_tag_cloud_widget($args) {
	$args['number'] = 0; //adding a 0 will display all tags
	$args['largest'] = 18; //largest tag
	$args['smallest'] = 12; //smallest tag
	$args['unit'] = 'px'; //tag font unit
	$args['format'] = 'list'; //ul with a class of wp-tag-cloud
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'unik_custom_tag_cloud_widget' );

/************************************************************/


/****************** ENABLE SESSIONS *************************/

function unik_admin_init() {
	if (!session_id())
	session_start();
}

//add_action('init', 'unik_admin_init');

/************************************************************/


add_theme_support( 'automatic-feed-links' );
if ( ! isset( $content_width ) ) $content_width = 960;

/************************************************************/

if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}

function unik_meta($post){
	
	$showdate = weblusive_get_option('blog_show_date'); 
	$showcomments = weblusive_get_option('blog_show_comments'); 
	$showauthor = weblusive_get_option('blog_show_author'); 
	$day = get_the_time('d'); $month = get_the_time('m'); $year = get_the_time('Y');
	if(!$showdate || !$showauthor || !$showcomments):?>
		<ul class="post-tags">
			<?php if(!$showdate): ?>
				<li>
					<i class="fa fa-calendar-o"></i>
					<a href="<?php echo  get_day_link( $year, $month, $day ); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a>
				</li>
			<?php endif; ?>
			<?php if(!$showauthor): ?>
				<li>
					<i class="fa fa-user"></i>
					<?php the_author_posts_link(); ?> 
				</li>
			<?php endif; ?>
			<?php if( 'open' == $post->comment_status && !$showcomments) : ?>
				 <li>
					 <i class="fa fa-comments"></i>                
					 <?php comments_popup_link( __( '0 comment', 'unik' ), __( '1 comment', 'unik' ), __( '% comments', 'unik' )); ?>                              
				 </li>
			<?php endif?>
		</ul>
	<?php endif;
}

/****************** WOOCommerce HOOKS ***********************/

function smarton_woopagination(){
	$perpage = weblusive_get_option('products_per_page');
	$prodperpage = 8;
	if (isset($perpage) && !empty($perpage)){
		$prodperpage =  $perpage;
	}
	return $prodperpage;
}

add_filter( 'loop_shop_per_page', 'smarton_woopagination', 20 );


/* Remove related products */
function wc_remove_related_products( $args ) {
	return array();
}

$hiderelated = weblusive_get_option('hide_related_products');
if ($hiderelated){
	add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 
}

function unik_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => '',
            'wrap_before' => '<ul class="page-tree" itemprop="breadcrumb">',
            'wrap_after'  => '</ul>',
            'before'      => '<li>',
            'after'       => '</li>',
            'home'        => _x( 'Home', 'breadcrumb', 'unik' ),
        );
}

add_filter( 'woocommerce_breadcrumb_defaults', 'unik_woocommerce_breadcrumbs' );

/********************* Visual Composer **********************/
require_once ($funcPath . '/shortcodes/shortcode.php');

/*
if (function_exists('vc_set_as_theme')) {
	vc_set_as_theme($disable_updater = false);
​}*/

if(function_exists('vc_remove_element')){
	vc_remove_element("vc_separator");
	vc_remove_element("vc_tabs");
}



//composer my shortcodes

if(function_exists('vc_map')){

	require_once (get_template_directory() .'/vc_templates/progress.php');
	require_once (get_template_directory() .'/vc_templates/button.php');
	require_once (get_template_directory() .'/vc_templates/alert.php');
	require_once (get_template_directory() .'/vc_templates/contact.php');
	require_once (get_template_directory() .'/vc_templates/sblock.php');
	require_once (get_template_directory() .'/vc_templates/fblock.php');
	require_once (get_template_directory() .'/vc_templates/tblock.php');
	require_once (get_template_directory() .'/vc_templates/modal.php');
	require_once (get_template_directory() .'/vc_templates/evideo.php');
	require_once (get_template_directory() .'/vc_templates/teammember.php');
	require_once (get_template_directory() .'/vc_templates/portlisting.php');
	require_once (get_template_directory() .'/vc_templates/bloglisting.php');
	require_once (get_template_directory() .'/vc_templates/panel.php');
	require_once (get_template_directory() .'/vc_templates/av_testimonial.php');
	require_once (get_template_directory() .'/vc_templates/circle.php');
	require_once (get_template_directory() .'/vc_templates/well.php');
	require_once (get_template_directory() .'/vc_templates/smicon.php');
	require_once (get_template_directory() .'/vc_templates/av_tabs.php');
	require_once (get_template_directory() .'/vc_templates/av_accordion.php');
	require_once (get_template_directory() .'/vc_templates/slider.php');
	require_once (get_template_directory() .'/vc_templates/boxlist.php');
	require_once (get_template_directory() .'/vc_templates/pricingtable.php');
	require_once (get_template_directory() .'/vc_templates/av_carousel.php');

}

/****************** TGM Activation *******************/

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

function my_theme_register_required_plugins() {

    $plugins = array(

		array(
            'name'      => 'WooCommerce',
            'slug'      => 'woocommerce',
            'required'  => false,
        ),
		array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => false,
        ),
		array(
			'name' => 'WPBakery Visual Composer', // The plugin name.
			'slug' => 'js_composer', // The plugin slug (typically the folder name).
			'source' => esc_url( get_template_directory_uri() . '/plugins/js_composer.zip' ), // The plugin source.
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
			'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
   	$config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',            
	);

    tgmpa( $plugins, $config );
}

/* ************************************************************************ */

if( class_exists("woocommerce") ) {

	if ( !function_exists('unik_loop_columns') ) :

		add_filter('loop_shop_columns', 'unik_loop_columns');

		function unik_loop_columns() {
			
			$percolumn = str_replace("columns-", "", weblusive_get_option('products_per_row') );
			if ( empty( $percolumn ) ) {
				$percolumn = 3;
			}

			return $percolumn; // 4 products per row
		}
	endif;
	
	if ( !function_exists('unik_related_products_args') ) :

		add_filter( 'woocommerce_output_related_products_args', 'unik_related_products_args' );

		function unik_related_products_args( $args ) {

			$args['posts_per_page'] = 3; // 4 related products
			$args['columns'] = 3; // arranged in 3 columns
			return $args;
		}
	endif;
}
?>