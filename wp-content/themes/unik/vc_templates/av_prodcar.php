<?php

/*******************************************/
vc_map( array(
   "name" => __("Woocommerce products listing", "unik"),
   "base" => "productcar",
   "class" => "",
   "icon" => "icon-wpb-my_productcar",
   'admin_enqueue_css' => array(get_template_directory_uri().'/vc_templates/shortcodes.css'),
   "category" => __('Content', "unik"),
   "params" => array(
       array(
         "type" => "dropdown",
         "class" => "",
         "heading" => __("Animation", "unik"),
         "param_name" => "anim",
         "value" => array( "None"=>"", "bounce"=>"bounce", "flash"=>"flash", "pulse"=>"pulse", "shake"=>"shake", "swing"=>"swing", "tada"=>"tada", "wobble"=>"wobble", "bounceIn"=>"bounceIn", "bounceInDown"=>"bounceInDown", "bounceInLeft"=>"bounceInLeft", "bounceInRight"=>"bounceInRight", "bounceInUp"=>"bounceInUp", "bounceOut"=>"bounceOut", "bounceOutDown"=>"bounceOutDown", "bounceOutLeft"=>"bounceOutLeft", "bounceOutRight"=>"bounceOutRight", "bounceOutUp"=>"bounceOutUp", "fadeIn"=>"fadeIn", "fadeInDown"=>"fadeInDown", "fadeInDownBig"=>"fadeInDownBig", "fadeInLeft"=>"fadeInLeft", "fadeInLeftBig"=>"fadeInLeftBig", "fadeInRight"=>"fadeInRight", "fadeInRightBig"=>"fadeInRightBig", "fadeInUp"=>"fadeInUp", "fadeInUpBig"=>"fadeInUpBig", "fadeOut"=>"fadeOut", "fadeOutDown"=>"fadeOutDown", "fadeOutDownBig"=>"fadeOutDownBig", "fadeOutLeft"=>"fadeOutLeft", "fadeOutLeftBig"=>"fadeOutLeftBig", "fadeOutRight"=>"fadeOutRight", "fadeOutRightBig"=>"fadeOutRightBig", "fadeOutUp"=>"fadeOutUp", "fadeOutUpBig"=>"fadeOutUpBig", "flip"=>"flip", "flipInX"=>"flipInX", "flipInY"=>"flipInY", "flipOutX"=>"flipOutX", "flipOutY"=>"flipOutY", "lightSpeedIn"=>"lightSpeedIn", "lightSpeedOut"=>"lightSpeedOut", "rotateIn"=>"rotateIn", "rotateInDownLeft"=>"rotateInDownLeft", "rotateInDownRight"=>"rotateInDownRight", "rotateInUpLeft"=>"rotateInUpLeft", "rotateInUpRight"=>"rotateInUpRight", "rotateOut"=>"rotateOut", "rotateOutDownLeft"=>"rotateOutDownLeft", "rotateOutDownRight"=>"rotateOutDownRight", "rotateOutUpLeft"=>"rotateOutUpLeft", "rotateOutUpRight"=>"rotateOutUpRight",  "hinge"=>"hinge", "rollIn"=>"rollIn", "rollOut"=>"rollOut", "zoomIn"=>"zoomIn","zoomInDown"=>"zoomInDown", "zoomInLeft"=>"zoomInLeft","zoomInRight"=>"zoomInRight","zoomInUp"=>"zoomInUp","zoomOut"=>"zoomOut", "zoomOutLeft"=>"zoomOutLeft", "zoomOutRight"=>"zoomOutRight","zoomOutUp"=>"zoomOutUp"),
         "description" => __(" Animation.", "unik")
      ),
	  array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Product ID(s)", "unik"),
         "param_name" => "prod_ids",
		 "description" => __('Separate product IDs with commas. E.g.: 4,18,22', "unik")
      ),
	  array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Product tag(s)", "unik"),
         "param_name" => "prod_tags",
		 "description" => __('Separate product tags with commas. E.g.: technology, furniture', "unik")
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Category ID(s)", "unik"),
         "param_name" => "cat_ids",
		 "description" => __('Separate category IDs with commas. E.g.: 4,18,22', "unik")
      ),
	  array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Category slug(s)", "unik"),
         "param_name" => "cat_slugs",
		 "description" => __('Separate category slugs with commas. E.g.: sofa,chair,table', "unik")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Items limit", "unik"),
         "param_name" => "limit",
		 "value"=>"12"
      ),
	   array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Items to Show", "unik"),
         "param_name" => "items",
		 "value"=>"4"
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Automatic sliding", "unik"),
         "param_name" => "automatic",
         "value"=>array("No"=>"false", "Yes"=>"true")
      ),
	    array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Sliding interval", "unik"),
         "param_name" => "interval",
		 "value"=>"2500"
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Show arrows", "unik"),
         "param_name" => "showarrows",
         "value"=>array("Yes"=>"true", "No"=>"false")
      ),
	    array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Order", "unik"),
         "param_name" => "order",
         "value"=>array("Descending"=>"DESC", "Ascending"=>"ASC")
      ),
	  array(
         "type" => "dropdown", 
         "class" => "",
         "heading" => __("Order by", "unik"),
         "param_name" => "orderby",
         "value"=>array("Default sorting"=>"menu_order", "Sort by popularity"=>"popularity", "Sort by average rating"=>"rating", "Sort by newness"=>"date", "Sort by price: low to high"=>"price", "Sort by price: high to low"=>"price-desc")
      ),
        array(
         "type" => "textfield",
         "class" => "",
         "heading" => __("Extra class", "unik"),
         "param_name" => "class",
         "description" => __(' Extra class name', "unik")
      )
   )
) );
?>
