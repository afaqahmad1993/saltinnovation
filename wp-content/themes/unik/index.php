<?php /* Main entry file */

get_header();
$catid = get_query_var('cat');
$cat = get_category($catid);
if ( get_query_var('paged') ) {

    $paged = get_query_var('paged');

} elseif ( get_query_var('page') ) {

    $paged = get_query_var('page');

} else {

    $paged = 1;

}
$blogLayout =  weblusive_get_option('blog_layout');
isset ($blogLayout) ? $blogLayout : $blogLayout==1;

$get_meta = get_post_custom($post->ID);
$weblusive_sidebar_pos = isset( $get_meta['_weblusive_sidebar_pos'][0]) ? $get_meta["_weblusive_sidebar_pos"][0] : 'full';
get_template_part( 'library/includes/page-head' ); 
get_template_part( 'inner-header', 'content'); 
?>

<div class="blog-section col1 page-section row">
	<?php if ($weblusive_sidebar_pos == 'left'):?><div class="col-md-4 side-div" ><?php get_sidebar(); ?></div><?php endif?>
	<div class="<?php if ($weblusive_sidebar_pos == 'full'):?>full-content<?php else:?>col-md-8<?php endif?>">

		<?php 
		$temp = $wp_query;
		$wp_query= null;
		$wp_query = new WP_Query();
		$pp = get_option('posts_per_page');
		$wp_query->query('posts_per_page='.$pp.'&paged='.$paged);			
		get_template_part( 'loop', 'index' );
		wp_link_pages();
		?>
	</div>
	<?php if ($weblusive_sidebar_pos == 'right'):?><div class="col-md-4 side-div" ><?php get_sidebar(); ?></div><?php endif?>
       
</div>
<?php if ( $wp_query->max_num_pages > 1 ): ?>
		<div class="pagination-list">
			<?php include(unik_PLUGINS . '/wp-pagenavi.php' ); wp_pagenavi(); ?> 
		</div>
	<?php endif?>
<?php get_footer(); ?>