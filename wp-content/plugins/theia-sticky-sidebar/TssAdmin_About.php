<?php
/*
 * Copyright 2013, Theia Sticky Sidebar, Liviu Cristian Mirea Ghiban.
 */

class TssAdmin_About {
	public function echoPage() {
		?>
		<p>
			Version: <?php echo TSS_VERSION ?><br>
			Author: Liviu Cristian Mirea Ghiban<br>
			Website: <a href="http://liviucmg.com">liviucmg.com</a><br>
			Email: <a href="mailto:contact@liviucmg.com">contact@liviucmg.com</a>
		</p>
		<?php
	}
}