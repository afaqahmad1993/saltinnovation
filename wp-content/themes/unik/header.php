<!DOCTYPE html>
<!--[if IE 8]> 	<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />  
	
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri() ?>/js/html5.js"></script>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/ie8.css" type="text/css" media="screen">
	<![endif]-->
	<?php if(weblusive_get_option('header_code')) echo  htmlspecialchars_decode(weblusive_get_option('header_code')); ?>
	<?php 
	(weblusive_get_option('theme_color')=='dark')? $bodyClass="dark" : $bodyClass='';
	//(isset($_GET['color']) && $_GET['color']=='dark')? $bodyClass="dark" : $bodyClass='';
	?>
	<?php wp_head(); ?>
</head>

<body <?php body_class($bodyClass); ?> >
	<?php 
	//for demo
	//(isset($_GET['menu']) && $_GET['menu']=='right')? $menuClass="right-menu-sidebar" : $menuClass='';
	$menuClass = weblusive_get_option('header_style');
	$sticky = weblusive_get_option('sticky_header');
	if($sticky) $sticky = 'fixed-head';
	
	?>
    <div id="container" class="container <?php echo $menuClass//echo weblusive_get_option('header_style') ?>" >
<?php if (!is_page_template('under-construction.php')):?>	
	<div id="sidebar" class="<?php echo $sticky?>">
		<header class="sidebar-section">
			<div class="header-logo">
				<?php 
					$logo = weblusive_get_option('logo'); 
					$logosettings =  weblusive_get_option('logo_setting');
				 ?>
				 <a href="<?php echo home_url() ?>" id="logo" class="logo">
					<?php $logo_size = ''; if($logosettings == 'logo' && !empty($logo)):
						if(function_exists('getimagesize')){
							$sizes = getimagesize($logo);
							$logo_size = $sizes[3];
						}
					?>
						<img src="<?php echo $logo ?>" alt="<?php echo get_bloginfo('name')?>" id="logo-image" <?php echo $logo_size?> />
					<?php else:?>
						<?php echo get_bloginfo('name') ?>
					<?php endif?>
				 </a>
			</div>
			<a class="elemadded responsive-link" href="#"><?php _e('Menu', 'unik')?></a>
			<div class="navbar-wrapper">
				<div class="navbar-vertical">
					<?php 
					$walker = new Unik_Menu_Walker;
					if( function_exists('wp_nav_menu') ):
						wp_nav_menu( 
							array( 
								'theme_location' => 'primary_nav',
								'menu' =>'primary_nav', 
								'container'=>'', 
								'depth' => 4, 
								'menu_class' => 'main-menu',
								'walker' => $walker
							)  
						);
					endif;
					?>
				</div>
			</div>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Primary Widget Area") ) :endif; ?>
		</header>
		
	</div>
	<div id="content">
		<?php endif;?>
		<?php wp_reset_query()?>